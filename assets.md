# HUGO SSG / Book template / Assets

- [_markdown.scsss](assets/_markdown.scss)
  - [x] Uitlijning tabellen horizontaal in het midden gezet
- [manifest.json](assets/manifest.json)
  - [x] Variabele $.Site.Params.flavicon Icons Flavicon toegevoegd als Site Configuratie optie voor een alternatieve Flavicon Icon
